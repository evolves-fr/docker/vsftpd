#!/bin/sh

for i in $USERS ; do
    NAME=$(echo $i | cut -d'|' -f1)
    PASS=$(echo $i | cut -d'|' -f2)
  FOLDER=$(echo $i | cut -d'|' -f3)
     UID=$(echo $i | cut -d'|' -f4)

  if [ -z "$FOLDER" ]; then
    FOLDER="/ftp/$NAME"
  fi

  if [ ! -z "$UID" ]; then
    UID_OPT="-u $UID"
  fi

  mkdir -p $(dirname "$FOLDER")
  adduser -D -h $FOLDER -s /sbin/nologin $UID_OPT $NAME
  echo "${NAME}:${PASS}" | chpasswd > /dev/null 2>&1
  unset NAME PASS FOLDER UID
done

envsubst < /etc/vsftpd/vsftpd.conf.template > /etc/vsftpd/vsftpd.conf

# Used to run custom commands inside container
if [ ! -z "$1" ]; then
  exec "$@"
else
  echo "Starting vsftpd"
  exec /usr/sbin/vsftpd /etc/vsftpd/vsftpd.conf
fi
