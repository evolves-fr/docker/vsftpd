FROM alpine:3.14.0

ARG COMMIT

LABEL org.opencontainers.image.title="vsftpd" \
      org.opencontainers.image.description="Probably the most secure and fastest FTP server for UNIX-like systems." \
      org.opencontainers.image.vendor="EVOLVES™ SAS" \
      org.opencontainers.image.authors="Thomas FOURNIER <tfournier@evolves.fr>" \
      org.opencontainers.image.source="https://gitlab.com/evolves-fr/docker/vsftpd" \
      org.opencontainers.image.version="3.0.4" \
      org.opencontainers.image.revision="$COMMIT" \
      org.opencontainers.image.licenses="GPL-2.0"

RUN apk --no-cache add vsftpd=3.0.4-r0 gettext=0.21-r0

COPY entrypoint.sh /usr/local/bin/entrypoint
COPY vsftpd.conf /etc/vsftpd/vsftpd.conf.template

STOPSIGNAL SIGKILL

ENV USERS="ftp|alpineftp" \
    ANONYMOUS_ENABLE="NO" \
    LOCAL_ENABLE="YES" \
    LOCAL_ROOT="" \
    WRITE_ENABLE="YES" \
    LOCAL_UMASK="022" \
    ANON_UPLOAD_ENABLE="NO" \
    ANON_MKDIR_WRITE_ENABLE="NO" \
    DIRMESSAGE_ENABLE="YES" \
    CONNECT_FROM_PORT_20="YES" \
    CHOWN_UPLOADS="NO" \
    CHOWN_USERNAME="root" \
    XFERLOG_ENABLE="YES" \
    XFERLOG_FILE="/var/log/xferlog" \
    XFERLOG_STD_FORMAT="NO" \
    IDLE_SESSION_TIMEOUT="300" \
    DATA_CONNECTION_TIMEOUT="300" \
    NOPRIV_USER="nobody" \
    ASYNC_ABOR_ENABLE="NO" \
    ASCII_UPLOAD_ENABLE="NO" \
    ASCII_DOWNLOAD_ENABLE="NO" \
    DENY_EMAIL_ENABLE="NO" \
    BANNED_EMAIL_FILE="/etc/vsftpd.banned_emails" \
    CHROOT_LOCAL_USER="NO" \
    CHROOT_LIST_ENABLE="NO" \
    CHROOT_LIST_FILE="/etc/vsftpd.chroot_list" \
    PASSWD_CHROOT_ENABLE="NO" \
    ALLOW_WRITEABLE_CHROOT="NO" \
    LS_RECURSE_ENABLE="NO" \
    FTPD_BANNER="Welcome to blah FTP service." \
    LISTEN="YES" \
    LISTEN_IPV6="NO" \
    PASV_ENABLE="YES" \
    PASV_PROMISCUOUS="NO" \
    PASV_MIN_PORT="30000" \
    PASV_MAX_PORT="30009" \
    PASV_ADDRESS="" \
    PASV_ADDR_RESOLVE="NO" \
    PORT_PROMISCUOUS="NO" \
    SECURE_CHROOT_DIR="/usr/share/empty" \
    USER_SUB_TOKEN="" \
    BACKGROUND="NO" \
    VSFTPD_LOG_FILE="/proc/1/fd/1" \
    SECCOMP_SANDBOX="NO" \
    SSL_ENABLE="NO" \
    FORCE_LOCAL_LOGINS_SSL="YES" \
    FORCE_LOCAL_DATA_SSL="YES" \
    RSA_CERT_FILE="/usr/share/ssl/certs/vsftpd.pem" \
    RSA_PRIVATE_KEY_FILE="" \
    REQUIRE_SSL_REUSE="YES" \
    SSL_CIPHERS="DES-CBC3-SHA"

EXPOSE 21 30000-30009
ENTRYPOINT ["/usr/local/bin/entrypoint"]
